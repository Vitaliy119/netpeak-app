import React from 'react';
import { PropTypes } from "prop-types";
import './post.scss'

export function Post ({ name }) {
    return (
        <div className='post-item'>
            <p className='post-item--name'>{name}</p>
        </div>
    )
}

Post.propTypes = {
    name: PropTypes.string
};

Post.defaultProps = {
    name: ''
};