import React from 'react';
import { PropTypes } from 'prop-types';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import './pagination.scss';

export function Pagination ({current, length, changesPage}) {

    return (
        <div className="pagination">
            {
                length.map((el,i) => {
                    let selected = '#9E9E9E';
                   if (current === el+1) {
                       selected = '#2196F3';
                   }

                   return <FloatingActionButton
                       backgroundColor={selected}
                       key={i}
                       onClick={() => changesPage(el + 1)}
                       mini={true}
                       style={{marginRight: '10px'}}
                   >
                            <div>{el + 1}</div>
                          </FloatingActionButton>
                })
            }
        </div>
    )
}

Pagination.propTypes = {
    current: PropTypes.number,
    length: PropTypes.array,
    changesPage: PropTypes.func
};

Pagination.defaultProps = {
    current: 0,
    length: []
};