'use strict';

import { combineReducers } from 'redux';
import { postReducer } from "./redusers/postReducer";

export default combineReducers({
    postReducer
});
