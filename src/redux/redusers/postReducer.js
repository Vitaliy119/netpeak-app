import { ALL_POST, CURRENT_PAGE } from "../../action/reducerConst";

const initialState = {
    allPosts: [],
    currentPage: 1
};

export function postReducer(state = initialState, action) {
    switch (action.type) {
        case ALL_POST:
            return {
                ...state,
                allPosts: action.payload
            };
            case CURRENT_PAGE:
            return {
                ...state,
                currentPage: action.payload
            };
            break;
        default: return state;
    }
}