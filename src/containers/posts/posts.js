import React from  'react';
import TextField from 'material-ui/TextField';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Post } from "../../components/post/post";
import { Pagination } from "../../components/pagination/pagination";
import { postList } from "../../action/posts";
import { CURRENT_PAGE } from "../../action/reducerConst";
import  './posts.scss';

class Posts extends React.Component {

    constructor (props) {
        super(props);
    }

    state = {
        searchValue: '',
        post: [],
        pages: [],
        current: this.props.currentPage,
        resultSearch:[]
    };

    componentDidMount() {
        (async () => {
            try {
                const result = await this.props.postList();
                const pages = Math.ceil(result.length / 10);
                const min = this.state.current * 10 - 10 ;
                const max = this.state.current * 10;
                let pagesLength = [];
                for(let i = 0; i < pages; i++) {
                    pagesLength.push(i);
                }
                this.setState({post: result.slice(min, max), pages: pagesLength});
            } catch (error) {
                console.log(error)
            }
        })();
    }

    componentDidUpdate() {
        let footer = document.querySelector('.pagination');
        footer.style.marginTop = 'auto';

        if (document.documentElement.offsetHeight < window.screen.availHeight) {
            const fmargin = document.documentElement.clientHeight - footer.offsetTop - footer.clientHeight;
            footer.style.marginTop = fmargin - 25 + 'px';
        } else {
            footer.style.marginTop = '50px';
        }
    }

    handleChange = (e) => {
        const resultSearch = this.props.allPosts.filter(el => el.title.includes(e.target.value));
        let pages = Math.ceil(resultSearch.length / 10);
        let pagesLength = [];
        if (pages > 1) {
            for(let i = 0; i < pages; i++) {
                pagesLength.push(i);
            }
        }
        this.setState({
            searchValue: e.target.value,
            post: resultSearch.slice(0, 10),
            pages: pagesLength,
            current: 1,
            resultSearch
        });
    };

    changesPage = (el) => {
        let result;
        const min = el * 10 - 10 ;
        const max = el * 10;
        this.props.page(el);
        if (this.state.searchValue) {
            result = this.state.resultSearch.slice(min, max);
        } else {
            result = this.props.allPosts.slice(min, max);
        }
        this.setState({current: el, post: result});
    };

    render () {
        return (
            <div className="post-list">
                <TextField
                    value={this.state.searchValue}
                    hintText="Search post"
                    floatingLabelText="Search post"
                    onChange={this.handleChange}
                />
                    {
                        this.state.post.map( el =>
                            <Link className='post-list__link' key={el.id} to={`post/${el.id}`}>
                                <Post
                                    name={el.title}
                                />
                            </Link>
                        )
                    }
                    <Pagination
                        length={this.state.pages}
                        current={this.state.current}
                        changesPage={this.changesPage}
                    />
            </div>

        )
    }
}

export default connect(
    state => ({
        allPosts: state.postReducer.allPosts,
        currentPage: state.postReducer.currentPage
    }),
    dispatch => ({
        postList: dispatch (postList),
        page: (el) => dispatch ({type: CURRENT_PAGE, payload: el})
    })
)(Posts)