import React, { Fragment } from 'react';
import FlatButton from 'material-ui/FlatButton';
import Backspace from 'material-ui/svg-icons/hardware/keyboard-backspace';
import { currentUser, postComment } from "../../action/posts";
import './selectedPost.scss';

export default class SelectedPost extends React.Component{
    constructor(props) {
        super(props)
    }

    state = {
        selectedPost: {},
        comments: []
    };

    componentDidMount() {
        (async () => {
            try {
                const result = await currentUser(this.props.match.params.id);
                const comments = await postComment(this.props.match.params.id);
                this.setState({selectedPost: result.data, comments: comments.data})
            } catch (error) {
                console.log(error)
            }
        })();
    }
    back = () => this.props.history.push('/');

    render() {
        return (
            <Fragment>
                <FlatButton
                    icon={<Backspace />}
                    label="Back"
                    onClick={this.back}
                />
                <div className="container">
                    <div className='selected-post'>
                        <h2 className='selected-post--header'>{this.state.selectedPost.title}</h2>
                        <p>{this.state.selectedPost.body}</p>
                    </div>
                    <h2>Comments</h2>
                    <hr/>
                    <div className='selected-post__comments'>
                        <dl>
                        {this.state.comments.map(el => {
                            return(
                                <Fragment key={el.id}>
                                    <dt className='selected-post--title'>{el.name}</dt>
                                    <dd className='selected-post--content'>{el.body}</dd>
                                </Fragment>
                            )
                        })}
                        </dl>
                    </div>
                </div>
            </Fragment>
        )
    }
}
