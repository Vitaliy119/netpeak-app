import React, { Fragment } from 'react';
import {Switch, Route } from 'react-router-dom';

import Posts from './containers/posts/posts'
import SelectedPost from "./containers/selectedPost/selectedPost";


export const Routers = () => (
    <Fragment>
        <Switch>
            <Route exact path="/" component={Posts} />
            <Route path="/post/:id" component={SelectedPost} />
        </Switch>
    </Fragment>
);

