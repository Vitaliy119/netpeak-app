import axios from "axios/index";
import { apiPath } from "./apiConst";
import { ALL_POST } from "./reducerConst";

export function postList(dispatch) {
    return async () => {
        try {
            const success = await axios.get(`${apiPath}/posts`);
            dispatch({ type: ALL_POST, payload: success.data });
            return success.data;
        } catch (error) {
            console.log(error);
        }
    }
}

export const currentUser = (id) => {
    return axios.get(`${apiPath}/posts/${id}`);
};

export const postComment = (id) => {
    return axios.get(`${apiPath}/posts/${id}/comments`);
};
