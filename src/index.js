'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import './styles/style.scss';
import {Routers} from './routers'
import {BrowserRouter} from 'react-router-dom';
import { applyMiddleware, createStore } from 'redux';
import { connectRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import reducer from './redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import { createBrowserHistory } from 'history';
import thunk from 'redux-thunk';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

const history = createBrowserHistory();
const store = createStore(connectRouter(history)(reducer), composeWithDevTools(applyMiddleware(thunk)));

ReactDOM.render((
  <Provider store={store}>
    <BrowserRouter>
      <MuiThemeProvider>
        <Routers/>
      </MuiThemeProvider>
    </BrowserRouter>
  </Provider>
), document.getElementById('root'));